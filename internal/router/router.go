package router

import (
	"erpsystemweb/internal/router/routes"

	"github.com/gorilla/mux"
)

func Hanlder() *mux.Router {
	r := mux.NewRouter()
	return routes.BootstrapRoutes(r)
}
