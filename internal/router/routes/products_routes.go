package routes

import (
	"erpsystemweb/internal/controllers"
	"erpsystemweb/internal/pages"
	"net/http"
)

var produtcsRoutes = []Route{
	{
		URI:     "/products",
		Method:  http.MethodGet,
		Func:    pages.ProductPage,
		HasAuth: false,
	},
	{
		URI:     "/products",
		Method:  http.MethodPost,
		Func:    controllers.CreateProduct,
		HasAuth: false,
	},
	{
		URI:     "/products/{productId}",
		Method:  http.MethodPut,
		Func:    controllers.UpdateProduct,
		HasAuth: false,
	},
	{
		URI:     "/products/{productId}",
		Method:  http.MethodDelete,
		Func:    controllers.DeleteProduct,
		HasAuth: false,
	},
	{
		URI:     "/products/{productId}/edit",
		Method:  http.MethodGet,
		Func:    pages.EditProductPage,
		HasAuth: false,
	},
}
