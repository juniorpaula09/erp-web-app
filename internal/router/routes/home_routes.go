package routes

import (
	"erpsystemweb/internal/pages"
	"net/http"
)

var homeRoutes = Route{
	URI:     "/home",
	Method:  http.MethodGet,
	Func:    pages.HomePage,
	HasAuth: false,
}
