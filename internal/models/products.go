package models

import "time"

type Products struct {
	ID          uint64    `json:"id,omitempty"`
	Cod         int       `json:"product_code"`
	Name        string    `json:"name"`
	Price       float64   `json:"price"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at,omitempty"`
}
