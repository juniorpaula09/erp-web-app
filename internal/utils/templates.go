package utils

import (
	"html/template"
	"net/http"
)

var templates *template.Template

func LoadTemplates() {
	templates = template.Must(templates.ParseGlob("web/views/*.html"))
	templates = template.Must(templates.ParseGlob("web/views/templates/*.html"))
}

func Render(w http.ResponseWriter, template string, data interface{}) {
	templates.ExecuteTemplate(w, template, data)
}
