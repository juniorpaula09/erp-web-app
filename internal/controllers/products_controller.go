package controllers

import (
	"bytes"
	"erpsystemweb/internal/config"
	"erpsystemweb/internal/request"
	"erpsystemweb/internal/responses"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func CreateProduct(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.WriteJSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/products", config.API_URL)
	resp, err := request.MakeRequest(r, http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		responses.VerifyStatusCodeErrors(w, resp)
		return
	}

	responses.WriteJSON(w, resp.StatusCode, nil)
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productID, err := strconv.ParseUint(params["productId"], 10, 64)
	if err != nil {
		responses.WriteJSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		responses.WriteJSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/products/%d", config.API_URL, productID)
	resp, err := request.MakeRequest(r, http.MethodPut, url, bytes.NewBuffer(body))
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode >= 400 {
		responses.VerifyStatusCodeErrors(w, resp)
		return
	}

	responses.WriteJSON(w, resp.StatusCode, nil)
}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productId, err := strconv.ParseUint(params["productId"], 10, 64)
	if err != nil {
		responses.WriteJSON(w, http.StatusBadRequest, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/products/%d", config.API_URL, productId)
	resp, err := request.MakeRequest(r, http.MethodDelete, url, nil)
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		responses.VerifyStatusCodeErrors(w, resp)
		return
	}

	responses.WriteJSON(w, resp.StatusCode, nil)
}
