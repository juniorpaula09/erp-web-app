package pages

import (
	"erpsystemweb/internal/utils"
	"net/http"
)

func HomePage(w http.ResponseWriter, r *http.Request) {
	utils.Render(w, "home.html", nil)
}
