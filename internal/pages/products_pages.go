package pages

import (
	"encoding/json"
	"erpsystemweb/internal/config"
	"erpsystemweb/internal/models"
	"erpsystemweb/internal/request"
	"erpsystemweb/internal/responses"
	"erpsystemweb/internal/utils"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func ProductPage(w http.ResponseWriter, r *http.Request) {
	url := fmt.Sprintf("%s/products", config.API_URL)
	resp, err := request.MakeRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		responses.VerifyStatusCodeErrors(w, resp)
		return
	}

	var products []models.Products
	if err = json.NewDecoder(resp.Body).Decode(&products); err != nil {
		responses.WriteJSON(w, http.StatusUnprocessableEntity, responses.ErrorAPI{Error: err.Error()})
		return
	}

	utils.Render(w, "products.html", struct {
		Products []models.Products
	}{
		Products: products,
	})
}

func EditProductPage(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productId, err := strconv.ParseUint(params["productId"], 10, 64)
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/products/%d", config.API_URL, productId)
	resp, err := request.MakeRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.WriteJSON(w, http.StatusInternalServerError, responses.ErrorAPI{Error: err.Error()})
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		responses.VerifyStatusCodeErrors(w, resp)
	}

	var product models.Products
	if err = json.NewDecoder(resp.Body).Decode(&product); err != nil {
		responses.WriteJSON(w, http.StatusUnprocessableEntity, responses.ErrorAPI{Error: err.Error()})
		return
	}

	utils.Render(w, "edit-product.html", product)
}
