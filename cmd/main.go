package main

import (
	"erpsystemweb/internal/config"
	"erpsystemweb/internal/router"
	"erpsystemweb/internal/utils"
	"fmt"
	"log"
	"net/http"
)

func main() {
	config.LoadEnv()
	utils.LoadTemplates()
	r := router.Hanlder()

	fmt.Printf("[::] web server app running at port %d\n", config.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), r))
}
